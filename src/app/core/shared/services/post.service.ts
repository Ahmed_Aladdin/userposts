import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }

  constructor(private http:HttpClient) { }

  getAllposts(userID){
    return this.http.get(`https://jsonplaceholder.typicode.com/posts?userId=${userID}`)
  }
  getPost(id){
    return this.http.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
  }
  deletePost(id){
    return this.http.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
  }
  updatePost(id,payload){
    return this.http.patch(`https://jsonplaceholder.typicode.com/posts/${id}`,payload,{headers:this.headers})
  }
}
